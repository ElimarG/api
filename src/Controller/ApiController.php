<?php

namespace App\Controller;

use App\Entity\Debt;
use App\Entity\Client;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
* @Route("/api", name="api")
*/
class ApiController extends FOSRestController
{
    
    /**
     * @Rest\Get("/v2/debt/{psfCode}.{_format}", name="debt_list", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Obtiene información de la deuda basada en el parámetro PSF_CODE pasado."
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="La deuda con el parámetro PSF_CODE pasado no se encontró o no existe."
     * )
     *
     * @SWG\Parameter(
     *     name="psfCode",
     *     in="path",
     *     type="string",
     *     description="The debt PSF_CODE"
     * )
     *
     *
     * @SWG\Tag(name="Client")
     */
    public function getDebtAction(Request $request, $psfCode) {
        $serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $debt = [];
        $message = "";
 
        try {
            $code = 200;
            $error = false;
 
            $psf_code = $psfCode;
            $debt = $em->getRepository("App:Debt");
            $debt = $debt->createQueryBuilder('d')
            ->leftJoin('d.client', 'c')
            ->where('c.psfCode = :psf_code')
            ->setParameter('psf_code', $psf_code)
            ->getQuery()
            ->getResult();
 
            if (is_null($debt)) {
                $code = 500;
                $error = true;
                $message = "La deuda no existe.";
            }
 
        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "Se ha producido un error intentando obtener la deuda actual. - Error: {$ex->getMessage()}";
        }
 
        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 200 ? $debt : $message,
        ];
 
        return new Response($serializer->serialize($response, "json"));
    }

    /**
     * @Rest\Post("/v2/debt.{_format}", name="debt_add", defaults={"_format":"json"})
     *
     * @SWG\Response(
     *     response=201,
     *     description="La deuda fue agregada exitosamente"
     * )
     *
     * @SWG\Response(
     *     response=500,
     *     description="Se ha producido un error intentando añadir nueva deuda"
     * )
     *
     * @SWG\Parameter(
     *     name="batchDate",
     *     in="body",
     *     type="string",
     *     description="La fecha de lote",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="numberDebts",
     *     in="body",
     *     type="string",
     *     description="La cantidad de deudas",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="totalAmount",
     *     in="body",
     *     type="string",
     *     description="El importe total",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="batchNumber",
     *     in="body",
     *     type="string",
     *     description="El numero de lote",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="psfCode",
     *     in="body",
     *     type="string",
     *     description="El codigo de pago sin factura",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="referenceNumber",
     *     in="body",
     *     type="string",
     *     description="El numero de referencia de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="observations",
     *     in="body",
     *     type="string",
     *     description="La observacion de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="expirationDate1",
     *     in="body",
     *     type="string",
     *     description="La fecha de vencimiento de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="amount1",
     *     in="body",
     *     type="string",
     *     description="El importe de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="expirationDate2",
     *     in="body",
     *     type="string",
     *     description="La segunda fecha de vencimiento de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="amount2",
     *     in="body",
     *     type="string",
     *     description="El segundo importe de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="expirationDate3",
     *     in="body",
     *     type="string",
     *     description="La tercera fecha de vencimiento de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Parameter(
     *     name="amount3",
     *     in="body",
     *     type="string",
     *     description="El tercer importe de la deuda",
     *     schema={}
     * )
     *
     * @SWG\Tag(name="Debt")
     */
    public function addDebtAction(Request $request) {
        $serializer = $this->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $debt = [];
        $message = "";
 
        try {
            $code = 201;
            $error = false;
            $batchNumber = $request->request->get("batch_number", null);
            $batchDate = $request->request->get("batch_date", null);
            $totalAmount = $request->request->get("total_amount", null);
            $numberDebts = $request->request->get("number_debts", null);
            $clientId= $request->request->get("client_id", null);
 
            if (!is_null($batchNumber) && !is_null($batchDate) && !is_null($totalAmount) && !is_null($numberDebts) && !is_null($clientId)) {
                $debt = new Debt();
                $client = $em->getRepository("App:Client")->find($clientId);
                $debt->setBoard($client);
                $debt->setTitle($batchNumber);
                $debt->setDescription($batchDate);
                $debt->setStatus($totalAmount);
                $debt->setPriority($numberDebts);
 
                $em->persist($debt);
                $em->flush();
 
            } else {
                $code = 500;
                $error = true;
                $message = "Se ha producido un error al intentar agregar una nueva deuda. Error: debe proporcionar todos los campos obligatorios";
            }
 
        } catch (Exception $ex) {
            $code = 500;
            $error = true;
            $message = "Se ha producido un error intentando añadir una nueva deuda. - Error: {$ex->getMessage()}";
        }
 
        $response = [
            'code' => $code,
            'error' => $error,
            'data' => $code == 201 ? $debt : $message,
        ];
 
        return new Response($serializer->serialize($response, "json"));
    }
}
