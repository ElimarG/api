<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * BatchProcess
 *
 * @ORM\Table(name="batch_process", indexes={@ORM\Index(name="IDX_D6CB92CC979B1AD6", columns={"company_id"})})
 * @ORM\Entity
 */
class BatchProcess
{
    /**
     * @var int
     *
     * @ORM\Column(name="batch_process_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $batchProcessId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="batch_number", type="integer", nullable=true)
     */
    private $batchNumber;

    /**
     * @var int|null
     *
     * @ORM\Column(name="number_debts", type="integer", nullable=true)
     */
    private $numberDebts;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="batch_date", type="datetime", nullable=true)
     */
    private $batchDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $totalAmount;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="evaluated", type="boolean", nullable=true)
     */
    private $evaluated;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="company_id")
     * })
     */
    private $company;

    public function getBatchProcessId(): ?int
    {
        return $this->batchProcessId;
    }

    public function getBatchNumber(): ?int
    {
        return $this->batchNumber;
    }

    public function setBatchNumber(?int $batchNumber): self
    {
        $this->batchNumber = $batchNumber;

        return $this;
    }

    public function getNumberDebts(): ?int
    {
        return $this->numberDebts;
    }

    public function setNumberDebts(?int $numberDebts): self
    {
        $this->numberDebts = $numberDebts;

        return $this;
    }

    public function getBatchDate(): ?\DateTimeInterface
    {
        return $this->batchDate;
    }

    public function setBatchDate(?\DateTimeInterface $batchDate): self
    {
        $this->batchDate = $batchDate;

        return $this;
    }

    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    public function setTotalAmount($totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getEvaluated(): ?bool
    {
        return $this->evaluated;
    }

    public function setEvaluated(?bool $evaluated): self
    {
        $this->evaluated = $evaluated;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }


}
