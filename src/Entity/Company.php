<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Company
 *
 * @ORM\Table(name="company", indexes={@ORM\Index(name="fk_company_implementation_type_id", columns={"implementation_type_id"})})
 * @ORM\Entity
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="company_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $companyId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact_email", type="string", length=255, nullable=true)
     */
    private $contactEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="process_email", type="string", length=255, nullable=true)
     */
    private $processEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entry_date", type="datetime", nullable=false)
     */
    private $entryDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var int
     *
     * @ORM\Column(name="max_batch_size", type="integer", nullable=false)
     */
    private $maxBatchSize;

    /**
     * @var int
     *
     * @ORM\Column(name="api_version", type="integer", nullable=false)
     */
    private $apiVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="tax_id", type="string", length=255, nullable=false)
     */
    private $taxId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="barcode_mask", type="string", length=255, nullable=true)
     */
    private $barcodeMask;

    /**
     * @var int|null
     *
     * @ORM\Column(name="max_batch_post_size", type="integer", nullable=true, options={"default"="1"})
     */
    private $maxBatchPostSize = '1';

    /**
     * @var \ImplementationType
     *
     * @ORM\ManyToOne(targetEntity="ImplementationType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="implementation_type_id", referencedColumnName="implementation_type_id")
     * })
     */
    private $implementationType;

    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getProcessEmail(): ?string
    {
        return $this->processEmail;
    }

    public function setProcessEmail(?string $processEmail): self
    {
        $this->processEmail = $processEmail;

        return $this;
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->entryDate;
    }

    public function setEntryDate(\DateTimeInterface $entryDate): self
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getMaxBatchSize(): ?int
    {
        return $this->maxBatchSize;
    }

    public function setMaxBatchSize(int $maxBatchSize): self
    {
        $this->maxBatchSize = $maxBatchSize;

        return $this;
    }

    public function getApiVersion(): ?int
    {
        return $this->apiVersion;
    }

    public function setApiVersion(int $apiVersion): self
    {
        $this->apiVersion = $apiVersion;

        return $this;
    }

    public function getTaxId(): ?string
    {
        return $this->taxId;
    }

    public function setTaxId(string $taxId): self
    {
        $this->taxId = $taxId;

        return $this;
    }

    public function getBarcodeMask(): ?string
    {
        return $this->barcodeMask;
    }

    public function setBarcodeMask(?string $barcodeMask): self
    {
        $this->barcodeMask = $barcodeMask;

        return $this;
    }

    public function getMaxBatchPostSize(): ?int
    {
        return $this->maxBatchPostSize;
    }

    public function setMaxBatchPostSize(?int $maxBatchPostSize): self
    {
        $this->maxBatchPostSize = $maxBatchPostSize;

        return $this;
    }

    public function getImplementationType(): ?ImplementationType
    {
        return $this->implementationType;
    }

    public function setImplementationType(?ImplementationType $implementationType): self
    {
        $this->implementationType = $implementationType;

        return $this;
    }


}
