<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CompanyBarcodeField
 *
 * @ORM\Table(name="company_barcode_field", indexes={@ORM\Index(name="fk_company_barcode_field_name_id", columns={"name_id"}), @ORM\Index(name="fk_company_barcode_field_company_entity_id", columns={"company_entity_id"})})
 * @ORM\Entity
 */
class CompanyBarcodeField
{
    /**
     * @var int
     *
     * @ORM\Column(name="barcode_field_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $barcodeFieldId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="from", type="integer", nullable=true)
     */
    private $from;

    /**
     * @var int|null
     *
     * @ORM\Column(name="to", type="integer", nullable=true)
     */
    private $to;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime", nullable=false)
     */
    private $modificationDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \CompanyEntity
     *
     * @ORM\ManyToOne(targetEntity="CompanyEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_entity_id", referencedColumnName="company_entity_id")
     * })
     */
    private $companyEntity;

    /**
     * @var \BarcodeFieldName
     *
     * @ORM\ManyToOne(targetEntity="BarcodeFieldName")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="name_id", referencedColumnName="name_id")
     * })
     */
    private $name;

    public function getBarcodeFieldId(): ?int
    {
        return $this->barcodeFieldId;
    }

    public function getFrom(): ?int
    {
        return $this->from;
    }

    public function setFrom(?int $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function getTo(): ?int
    {
        return $this->to;
    }

    public function setTo(?int $to): self
    {
        $this->to = $to;

        return $this;
    }

    public function getModificationDate(): ?\DateTimeInterface
    {
        return $this->modificationDate;
    }

    public function setModificationDate(\DateTimeInterface $modificationDate): self
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCompanyEntity(): ?CompanyEntity
    {
        return $this->companyEntity;
    }

    public function setCompanyEntity(?CompanyEntity $companyEntity): self
    {
        $this->companyEntity = $companyEntity;

        return $this;
    }

    public function getName(): ?BarcodeFieldName
    {
        return $this->name;
    }

    public function setName(?BarcodeFieldName $name): self
    {
        $this->name = $name;

        return $this;
    }


}
