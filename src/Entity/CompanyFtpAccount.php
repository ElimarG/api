<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CompanyFtpAccount
 *
 * @ORM\Table(name="company_ftp_account", indexes={@ORM\Index(name="fk_company_ftp_account_encryption_id", columns={"encryption_id"}), @ORM\Index(name="fk_company_ftp_account_company_id", columns={"company_id"}), @ORM\Index(name="fk_company_ftp_account_ftp_account_type_id", columns={"ftp_account_type_id"})})
 * @ORM\Entity
 */
class CompanyFtpAccount
{
    /**
     * @var int
     *
     * @ORM\Column(name="ftp_account_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $ftpAccountId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="folder", type="text", length=0, nullable=true)
     */
    private $folder;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var int|null
     *
     * @ORM\Column(name="port", type="integer", nullable=true)
     */
    private $port;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user", type="string", length=255, nullable=true)
     */
    private $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password", type="text", length=0, nullable=true)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="file_key", type="text", length=0, nullable=true)
     */
    private $fileKey;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="company_id")
     * })
     */
    private $company;

    /**
     * @var \FtpAccountEncryption
     *
     * @ORM\ManyToOne(targetEntity="FtpAccountEncryption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="encryption_id", referencedColumnName="encryption_id")
     * })
     */
    private $encryption;

    /**
     * @var \FtpAccountType
     *
     * @ORM\ManyToOne(targetEntity="FtpAccountType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ftp_account_type_id", referencedColumnName="ftp_account_type_id")
     * })
     */
    private $ftpAccountType;

    public function getFtpAccountId(): ?int
    {
        return $this->ftpAccountId;
    }

    public function getFolder(): ?string
    {
        return $this->folder;
    }

    public function setFolder(?string $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function setPort(?int $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(?string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFileKey(): ?string
    {
        return $this->fileKey;
    }

    public function setFileKey(?string $fileKey): self
    {
        $this->fileKey = $fileKey;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getEncryption(): ?FtpAccountEncryption
    {
        return $this->encryption;
    }

    public function setEncryption(?FtpAccountEncryption $encryption): self
    {
        $this->encryption = $encryption;

        return $this;
    }

    public function getFtpAccountType(): ?FtpAccountType
    {
        return $this->ftpAccountType;
    }

    public function setFtpAccountType(?FtpAccountType $ftpAccountType): self
    {
        $this->ftpAccountType = $ftpAccountType;

        return $this;
    }


}
