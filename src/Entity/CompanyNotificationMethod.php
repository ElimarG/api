<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CompanyNotificationMethod
 *
 * @ORM\Table(name="company_notification_method", indexes={@ORM\Index(name="fk_company_notification_method_notification_method_id", columns={"notification_method_id"}), @ORM\Index(name="fk_company_notification_methdo_company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class CompanyNotificationMethod
{
    /**
     * @var int
     *
     * @ORM\Column(name="company_notification_method_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $companyNotificationMethodId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entry_date", type="datetime", nullable=false)
     */
    private $entryDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="company_id")
     * })
     */
    private $company;

    /**
     * @var \NotificationMethod
     *
     * @ORM\ManyToOne(targetEntity="NotificationMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_method_id", referencedColumnName="notification_method_id")
     * })
     */
    private $notificationMethod;

    public function getCompanyNotificationMethodId(): ?int
    {
        return $this->companyNotificationMethodId;
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->entryDate;
    }

    public function setEntryDate(\DateTimeInterface $entryDate): self
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getNotificationMethod(): ?NotificationMethod
    {
        return $this->notificationMethod;
    }

    public function setNotificationMethod(?NotificationMethod $notificationMethod): self
    {
        $this->notificationMethod = $notificationMethod;

        return $this;
    }


}
