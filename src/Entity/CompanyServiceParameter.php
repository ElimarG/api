<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CompanyServiceParameter
 *
 * @ORM\Table(name="company_service_parameter", uniqueConstraints={@ORM\UniqueConstraint(name="unique_parameter", columns={"service_parameter_id", "company_service_id"})}, indexes={@ORM\Index(name="IDX_2E9B3AA2A3FAAE33", columns={"service_parameter_id"}), @ORM\Index(name="IDX_2E9B3AA250D24070", columns={"company_service_id"})})
 * @ORM\Entity
 */
class CompanyServiceParameter
{
    /**
     * @var int
     *
     * @ORM\Column(name="company_service_parameter_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $companyServiceParameterId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \CompanyService
     *
     * @ORM\ManyToOne(targetEntity="CompanyService")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_service_id", referencedColumnName="company_service_id")
     * })
     */
    private $companyService;

    /**
     * @var \ServiceParameter
     *
     * @ORM\ManyToOne(targetEntity="ServiceParameter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="service_parameter_id", referencedColumnName="service_parameter_id")
     * })
     */
    private $serviceParameter;

    public function getCompanyServiceParameterId(): ?int
    {
        return $this->companyServiceParameterId;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCompanyService(): ?CompanyService
    {
        return $this->companyService;
    }

    public function setCompanyService(?CompanyService $companyService): self
    {
        $this->companyService = $companyService;

        return $this;
    }

    public function getServiceParameter(): ?ServiceParameter
    {
        return $this->serviceParameter;
    }

    public function setServiceParameter(?ServiceParameter $serviceParameter): self
    {
        $this->serviceParameter = $serviceParameter;

        return $this;
    }


}
