<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CompanyUrl
 *
 * @ORM\Table(name="company_url", indexes={@ORM\Index(name="fk_company_url_url_type_id", columns={"url_type_id"}), @ORM\Index(name="fk_company_url_company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class CompanyUrl
{
    /**
     * @var int
     *
     * @ORM\Column(name="company_url_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $companyUrlId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entry_date", type="datetime", nullable=false)
     */
    private $entryDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="company_id")
     * })
     */
    private $company;

    /**
     * @var \UrlType
     *
     * @ORM\ManyToOne(targetEntity="UrlType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="url_type_id", referencedColumnName="url_type_id")
     * })
     */
    private $urlType;

    public function getCompanyUrlId(): ?int
    {
        return $this->companyUrlId;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->entryDate;
    }

    public function setEntryDate(\DateTimeInterface $entryDate): self
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getUrlType(): ?UrlType
    {
        return $this->urlType;
    }

    public function setUrlType(?UrlType $urlType): self
    {
        $this->urlType = $urlType;

        return $this;
    }


}
