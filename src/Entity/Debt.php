<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Debt
 *
 * @ORM\Table(name="debt", indexes={@ORM\Index(name="debt_debt_batch_id", columns={"debt_batch_id"}), @ORM\Index(name="IDX_DBBF0A836BF700BD", columns={"status_id"}), @ORM\Index(name="debt_client_id", columns={"client_id"}), @ORM\Index(name="debt_debt_motive_id", columns={"debt_motive_id"})})
 * @ORM\Entity
 */
class Debt
{
    /**
     * @var int
     *
     * @ORM\Column(name="debt_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $debtId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reference_number", type="string", length=255, nullable=true)
     */
    private $referenceNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observations", type="string", length=255, nullable=true)
     */
    private $observations;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expiration_date1", type="datetime", nullable=true)
     */
    private $expirationDate1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount1", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $amount1;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expiration_date2", type="datetime", nullable=true)
     */
    private $expirationDate2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount2", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $amount2;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expiration_date3", type="datetime", nullable=true)
     */
    private $expirationDate3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount3", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $amount3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="barcode", type="string", length=60, nullable=true)
     */
    private $barcode;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="client_id")
     * })
     */
    private $client;

    /**
     * @var \DebtBatch
     *
     * @ORM\ManyToOne(targetEntity="DebtBatch")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="debt_batch_id", referencedColumnName="debt_batch_id")
     * })
     */
    private $debtBatch;

    /**
     * @var \DebtMotive
     *
     * @ORM\ManyToOne(targetEntity="DebtMotive")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="debt_motive_id", referencedColumnName="debt_motive_id")
     * })
     */
    private $debtMotive;

    /**
     * @var \DebtStatus
     *
     * @ORM\ManyToOne(targetEntity="DebtStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="status_id")
     * })
     */
    private $status;

    public function getDebtId(): ?int
    {
        return $this->debtId;
    }

    public function getReferenceNumber(): ?string
    {
        return $this->referenceNumber;
    }

    public function setReferenceNumber(?string $referenceNumber): self
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getExpirationDate1(): ?\DateTimeInterface
    {
        return $this->expirationDate1;
    }

    public function setExpirationDate1(?\DateTimeInterface $expirationDate1): self
    {
        $this->expirationDate1 = $expirationDate1;

        return $this;
    }

    public function getAmount1()
    {
        return $this->amount1;
    }

    public function setAmount1($amount1): self
    {
        $this->amount1 = $amount1;

        return $this;
    }

    public function getExpirationDate2(): ?\DateTimeInterface
    {
        return $this->expirationDate2;
    }

    public function setExpirationDate2(?\DateTimeInterface $expirationDate2): self
    {
        $this->expirationDate2 = $expirationDate2;

        return $this;
    }

    public function getAmount2()
    {
        return $this->amount2;
    }

    public function setAmount2($amount2): self
    {
        $this->amount2 = $amount2;

        return $this;
    }

    public function getExpirationDate3(): ?\DateTimeInterface
    {
        return $this->expirationDate3;
    }

    public function setExpirationDate3(?\DateTimeInterface $expirationDate3): self
    {
        $this->expirationDate3 = $expirationDate3;

        return $this;
    }

    public function getAmount3()
    {
        return $this->amount3;
    }

    public function setAmount3($amount3): self
    {
        $this->amount3 = $amount3;

        return $this;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(?string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getDebtBatch(): ?DebtBatch
    {
        return $this->debtBatch;
    }

    public function setDebtBatch(?DebtBatch $debtBatch): self
    {
        $this->debtBatch = $debtBatch;

        return $this;
    }

    public function getDebtMotive(): ?DebtMotive
    {
        return $this->debtMotive;
    }

    public function setDebtMotive(?DebtMotive $debtMotive): self
    {
        $this->debtMotive = $debtMotive;

        return $this;
    }

    public function getStatus(): ?DebtStatus
    {
        return $this->status;
    }

    public function setStatus(?DebtStatus $status): self
    {
        $this->status = $status;

        return $this;
    }


}
