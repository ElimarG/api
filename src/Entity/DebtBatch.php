<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * DebtBatch
 *
 * @ORM\Table(name="debt_batch", indexes={@ORM\Index(name="fk_debt_batch_debt_batch_process_id", columns={"debt_batch_process_id"})})
 * @ORM\Entity
 */
class DebtBatch
{
    /**
     * @var int
     *
     * @ORM\Column(name="debt_batch_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $debtBatchId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="batch_date", type="datetime", nullable=true)
     */
    private $batchDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="number_debts", type="integer", nullable=true)
     */
    private $numberDebts;

    /**
     * @var string|null
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=12, scale=9, nullable=true)
     */
    private $totalAmount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="batch_number", type="integer", nullable=true)
     */
    private $batchNumber;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="informed_date", type="datetime", nullable=true)
     */
    private $informedDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \BatchProcess
     *
     * @ORM\ManyToOne(targetEntity="BatchProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="debt_batch_process_id", referencedColumnName="batch_process_id")
     * })
     */
    private $debtBatchProcess;

    public function getDebtBatchId(): ?int
    {
        return $this->debtBatchId;
    }

    public function getBatchDate(): ?\DateTimeInterface
    {
        return $this->batchDate;
    }

    public function setBatchDate(?\DateTimeInterface $batchDate): self
    {
        $this->batchDate = $batchDate;

        return $this;
    }

    public function getNumberDebts(): ?int
    {
        return $this->numberDebts;
    }

    public function setNumberDebts(?int $numberDebts): self
    {
        $this->numberDebts = $numberDebts;

        return $this;
    }

    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    public function setTotalAmount($totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getBatchNumber(): ?int
    {
        return $this->batchNumber;
    }

    public function setBatchNumber(?int $batchNumber): self
    {
        $this->batchNumber = $batchNumber;

        return $this;
    }

    public function getInformedDate(): ?\DateTimeInterface
    {
        return $this->informedDate;
    }

    public function setInformedDate(?\DateTimeInterface $informedDate): self
    {
        $this->informedDate = $informedDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDebtBatchProcess(): ?BatchProcess
    {
        return $this->debtBatchProcess;
    }

    public function setDebtBatchProcess(?BatchProcess $debtBatchProcess): self
    {
        $this->debtBatchProcess = $debtBatchProcess;

        return $this;
    }


}
