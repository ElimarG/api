<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * DebtEntityBatch
 *
 * @ORM\Table(name="debt_entity_batch", indexes={@ORM\Index(name="fk_debt_entity_batch_company_id_idx", columns={"company_id"})})
 * @ORM\Entity
 */
class DebtEntityBatch
{
    /**
     * @var int
     *
     * @ORM\Column(name="debt_entity_batch_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $debtEntityBatchId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="batch_date", type="datetime", nullable=false)
     */
    private $batchDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="number_debts", type="integer", nullable=true)
     */
    private $numberDebts;

    /**
     * @var string|null
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $totalAmount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="batch_number", type="integer", nullable=true)
     */
    private $batchNumber;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="company_id")
     * })
     */
    private $company;

    public function getDebtEntityBatchId(): ?int
    {
        return $this->debtEntityBatchId;
    }

    public function getBatchDate(): ?\DateTimeInterface
    {
        return $this->batchDate;
    }

    public function setBatchDate(\DateTimeInterface $batchDate): self
    {
        $this->batchDate = $batchDate;

        return $this;
    }

    public function getNumberDebts(): ?int
    {
        return $this->numberDebts;
    }

    public function setNumberDebts(?int $numberDebts): self
    {
        $this->numberDebts = $numberDebts;

        return $this;
    }

    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    public function setTotalAmount($totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getBatchNumber(): ?int
    {
        return $this->batchNumber;
    }

    public function setBatchNumber(?int $batchNumber): self
    {
        $this->batchNumber = $batchNumber;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }


}
