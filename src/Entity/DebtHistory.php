<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * DebtHistory
 *
 * @ORM\Table(name="debt_history", indexes={@ORM\Index(name="fk_debt_history_debt_status_idx", columns={"new_debt_status"}), @ORM\Index(name="fk_debt_history_debt_id_idx", columns={"debt_id"}), @ORM\Index(name="fk_debt_history_debt_entity_batch_id_idx", columns={"debt_entity_batch_id"})})
 * @ORM\Entity
 */
class DebtHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="debt_history_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $debtHistoryId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="entry_date", type="datetime", nullable=true)
     */
    private $entryDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Debt
     *
     * @ORM\ManyToOne(targetEntity="Debt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="debt_id", referencedColumnName="debt_id")
     * })
     */
    private $debt;

    /**
     * @var \DebtStatus
     *
     * @ORM\ManyToOne(targetEntity="DebtStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="new_debt_status", referencedColumnName="status_id")
     * })
     */
    private $newDebtStatus;

    /**
     * @var \DebtEntityBatch
     *
     * @ORM\ManyToOne(targetEntity="DebtEntityBatch")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="debt_entity_batch_id", referencedColumnName="debt_entity_batch_id")
     * })
     */
    private $debtEntityBatch;

    public function getDebtHistoryId(): ?int
    {
        return $this->debtHistoryId;
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->entryDate;
    }

    public function setEntryDate(?\DateTimeInterface $entryDate): self
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDebt(): ?Debt
    {
        return $this->debt;
    }

    public function setDebt(?Debt $debt): self
    {
        $this->debt = $debt;

        return $this;
    }

    public function getNewDebtStatus(): ?DebtStatus
    {
        return $this->newDebtStatus;
    }

    public function setNewDebtStatus(?DebtStatus $newDebtStatus): self
    {
        $this->newDebtStatus = $newDebtStatus;

        return $this;
    }

    public function getDebtEntityBatch(): ?DebtEntityBatch
    {
        return $this->debtEntityBatch;
    }

    public function setDebtEntityBatch(?DebtEntityBatch $debtEntityBatch): self
    {
        $this->debtEntityBatch = $debtEntityBatch;

        return $this;
    }


}
