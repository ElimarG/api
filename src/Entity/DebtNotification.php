<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * DebtNotification
 *
 * @ORM\Table(name="debt_notification", indexes={@ORM\Index(name="fk_debt_notification_notification_method_id", columns={"notification_method_id"}), @ORM\Index(name="IDX_791A4B2081257D5D", columns={"entity_id"}), @ORM\Index(name="fk_debt_notification_debt_entity_batch_id", columns={"debt_entity_batch_id"})})
 * @ORM\Entity
 */
class DebtNotification
{
    /**
     * @var int
     *
     * @ORM\Column(name="debt_notification_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $debtNotificationId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="informed_date", type="datetime", nullable=true)
     */
    private $informedDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Entity
     *
     * @ORM\ManyToOne(targetEntity="Entity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="entity_id")
     * })
     */
    private $entity;

    /**
     * @var \NotificationMethod
     *
     * @ORM\ManyToOne(targetEntity="NotificationMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_method_id", referencedColumnName="notification_method_id")
     * })
     */
    private $notificationMethod;

    /**
     * @var \DebtEntityBatch
     *
     * @ORM\ManyToOne(targetEntity="DebtEntityBatch")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="debt_entity_batch_id", referencedColumnName="debt_entity_batch_id")
     * })
     */
    private $debtEntityBatch;

    public function getDebtNotificationId(): ?int
    {
        return $this->debtNotificationId;
    }

    public function getInformedDate(): ?\DateTimeInterface
    {
        return $this->informedDate;
    }

    public function setInformedDate(?\DateTimeInterface $informedDate): self
    {
        $this->informedDate = $informedDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getEntity(): ?Entity
    {
        return $this->entity;
    }

    public function setEntity(?Entity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getNotificationMethod(): ?NotificationMethod
    {
        return $this->notificationMethod;
    }

    public function setNotificationMethod(?NotificationMethod $notificationMethod): self
    {
        $this->notificationMethod = $notificationMethod;

        return $this;
    }

    public function getDebtEntityBatch(): ?DebtEntityBatch
    {
        return $this->debtEntityBatch;
    }

    public function setDebtEntityBatch(?DebtEntityBatch $debtEntityBatch): self
    {
        $this->debtEntityBatch = $debtEntityBatch;

        return $this;
    }


}
