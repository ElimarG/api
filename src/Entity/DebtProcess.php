<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * DebtProcess
 *
 * @ORM\Table(name="debt_process", indexes={@ORM\Index(name="fk_debt_process_user_id_idx", columns={"user_process_id"})})
 * @ORM\Entity
 */
class DebtProcess
{
    /**
     * @var int
     *
     * @ORM\Column(name="debt_process_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $debtProcessId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reference_number", type="string", length=45, nullable=true)
     */
    private $referenceNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="expiration_date1", type="string", length=45, nullable=true)
     */
    private $expirationDate1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount1", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $amount1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="expiration_date2", type="string", length=45, nullable=true)
     */
    private $expirationDate2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount2", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $amount2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="expiration_date3", type="string", length=45, nullable=true)
     */
    private $expirationDate3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount3", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $amount3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observations", type="string", length=100, nullable=true)
     */
    private $observations;

    /**
     * @var \UserProcess
     *
     * @ORM\ManyToOne(targetEntity="UserProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_process_id", referencedColumnName="user_process_id")
     * })
     */
    private $userProcess;

    public function getDebtProcessId(): ?int
    {
        return $this->debtProcessId;
    }

    public function getReferenceNumber(): ?string
    {
        return $this->referenceNumber;
    }

    public function setReferenceNumber(?string $referenceNumber): self
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    public function getExpirationDate1(): ?string
    {
        return $this->expirationDate1;
    }

    public function setExpirationDate1(?string $expirationDate1): self
    {
        $this->expirationDate1 = $expirationDate1;

        return $this;
    }

    public function getAmount1()
    {
        return $this->amount1;
    }

    public function setAmount1($amount1): self
    {
        $this->amount1 = $amount1;

        return $this;
    }

    public function getExpirationDate2(): ?string
    {
        return $this->expirationDate2;
    }

    public function setExpirationDate2(?string $expirationDate2): self
    {
        $this->expirationDate2 = $expirationDate2;

        return $this;
    }

    public function getAmount2()
    {
        return $this->amount2;
    }

    public function setAmount2($amount2): self
    {
        $this->amount2 = $amount2;

        return $this;
    }

    public function getExpirationDate3(): ?string
    {
        return $this->expirationDate3;
    }

    public function setExpirationDate3(?string $expirationDate3): self
    {
        $this->expirationDate3 = $expirationDate3;

        return $this;
    }

    public function getAmount3()
    {
        return $this->amount3;
    }

    public function setAmount3($amount3): self
    {
        $this->amount3 = $amount3;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getUserProcess(): ?UserProcess
    {
        return $this->userProcess;
    }

    public function setUserProcess(?UserProcess $userProcess): self
    {
        $this->userProcess = $userProcess;

        return $this;
    }


}
