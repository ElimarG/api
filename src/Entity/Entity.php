<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Entity
 *
 * @ORM\Table(name="entity")
 * @ORM\Entity
 */
class Entity
{
    /**
     * @var int
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $entityId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="entity_code", type="string", length=255, nullable=true)
     */
    private $entityCode;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="push_enabled", type="boolean", nullable=false)
     */
    private $pushEnabled;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url_detail", type="string", length=255, nullable=true)
     */
    private $urlDetail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="header_detail", type="string", length=255, nullable=true)
     */
    private $headerDetail;

    public function getEntityId(): ?int
    {
        return $this->entityId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEntityCode(): ?string
    {
        return $this->entityCode;
    }

    public function setEntityCode(?string $entityCode): self
    {
        $this->entityCode = $entityCode;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getPushEnabled(): ?bool
    {
        return $this->pushEnabled;
    }

    public function setPushEnabled(bool $pushEnabled): self
    {
        $this->pushEnabled = $pushEnabled;

        return $this;
    }

    public function getUrlDetail(): ?string
    {
        return $this->urlDetail;
    }

    public function setUrlDetail(?string $urlDetail): self
    {
        $this->urlDetail = $urlDetail;

        return $this;
    }

    public function getHeaderDetail(): ?string
    {
        return $this->headerDetail;
    }

    public function setHeaderDetail(?string $headerDetail): self
    {
        $this->headerDetail = $headerDetail;

        return $this;
    }


}
