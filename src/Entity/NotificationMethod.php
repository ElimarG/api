<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * NotificationMethod
 *
 * @ORM\Table(name="notification_method")
 * @ORM\Entity
 */
class NotificationMethod
{
    /**
     * @var int
     *
     * @ORM\Column(name="notification_method_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $notificationMethodId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var int
     *
     * @ORM\Column(name="batch_type_id", type="integer", nullable=false)
     */
    private $batchTypeId;

    public function getNotificationMethodId(): ?int
    {
        return $this->notificationMethodId;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getBatchTypeId(): ?int
    {
        return $this->batchTypeId;
    }

    public function setBatchTypeId(int $batchTypeId): self
    {
        $this->batchTypeId = $batchTypeId;

        return $this;
    }


}
