<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Payment
 *
 * @ORM\Table(name="payment", indexes={@ORM\Index(name="fk_payment_payment_method_id", columns={"payment_method_id"}), @ORM\Index(name="IDX_6D28840D240326A5", columns={"debt_id"}), @ORM\Index(name="fk_payment_client_id", columns={"client_id"}), @ORM\Index(name="fk_payment_payment_status", columns={"payment_status_id"}), @ORM\Index(name="fk_payment_date", columns={"payment_date"})})
 * @ORM\Entity
 */
class Payment
{
    /**
     * @var int
     *
     * @ORM\Column(name="payment_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="barcode", type="string", length=255, nullable=true)
     */
    private $barcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="number_id", type="string", length=255, nullable=true)
     */
    private $numberId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="payment_date", type="datetime", nullable=true)
     */
    private $paymentDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="reverse_date", type="datetime", nullable=true)
     */
    private $reverseDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="amount", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="terminal", type="string", length=255, nullable=true)
     */
    private $terminal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="operation_number", type="string", length=255, nullable=true)
     */
    private $operationNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observations", type="string", length=255, nullable=true)
     */
    private $observations;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var string|null
     *
     * @ORM\Column(name="payment_reference", type="string", length=255, nullable=true)
     */
    private $paymentReference;

    /**
     * @var int|null
     *
     * @ORM\Column(name="external_id", type="integer", nullable=true)
     */
    private $externalId;

    /**
     * @var \Debt
     *
     * @ORM\ManyToOne(targetEntity="Debt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="debt_id", referencedColumnName="debt_id")
     * })
     */
    private $debt;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="client_id")
     * })
     */
    private $client;

    /**
     * @var \PaymentMethod
     *
     * @ORM\ManyToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_method_id", referencedColumnName="payment_method_id")
     * })
     */
    private $paymentMethod;

    /**
     * @var \PaymentStatus
     *
     * @ORM\ManyToOne(targetEntity="PaymentStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_status_id", referencedColumnName="payment_status_id")
     * })
     */
    private $paymentStatus;

    public function getPaymentId(): ?int
    {
        return $this->paymentId;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(?string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getNumberId(): ?string
    {
        return $this->numberId;
    }

    public function setNumberId(?string $numberId): self
    {
        $this->numberId = $numberId;

        return $this;
    }

    public function getPaymentDate(): ?\DateTimeInterface
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(?\DateTimeInterface $paymentDate): self
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    public function getReverseDate(): ?\DateTimeInterface
    {
        return $this->reverseDate;
    }

    public function setReverseDate(?\DateTimeInterface $reverseDate): self
    {
        $this->reverseDate = $reverseDate;

        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTerminal(): ?string
    {
        return $this->terminal;
    }

    public function setTerminal(?string $terminal): self
    {
        $this->terminal = $terminal;

        return $this;
    }

    public function getOperationNumber(): ?string
    {
        return $this->operationNumber;
    }

    public function setOperationNumber(?string $operationNumber): self
    {
        $this->operationNumber = $operationNumber;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getPaymentReference(): ?string
    {
        return $this->paymentReference;
    }

    public function setPaymentReference(?string $paymentReference): self
    {
        $this->paymentReference = $paymentReference;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function setExternalId(?int $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getDebt(): ?Debt
    {
        return $this->debt;
    }

    public function setDebt(?Debt $debt): self
    {
        $this->debt = $debt;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getPaymentStatus(): ?PaymentStatus
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(?PaymentStatus $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }


}
