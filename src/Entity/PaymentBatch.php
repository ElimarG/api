<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PaymentBatch
 *
 * @ORM\Table(name="payment_batch", indexes={@ORM\Index(name="fk_payment_batch_batch_type_id_idx", columns={"batch_type_id"}), @ORM\Index(name="fk_payment_batch_company_id_idx", columns={"company_id"})})
 * @ORM\Entity
 */
class PaymentBatch
{
    /**
     * @var int
     *
     * @ORM\Column(name="payment_batch_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentBatchId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="batch_date", type="datetime", nullable=false)
     */
    private $batchDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="number_payments", type="integer", nullable=true)
     */
    private $numberPayments;

    /**
     * @var string|null
     *
     * @ORM\Column(name="total_amount", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $totalAmount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="batch_number", type="integer", nullable=true)
     */
    private $batchNumber;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \BatchType
     *
     * @ORM\ManyToOne(targetEntity="BatchType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="batch_type_id", referencedColumnName="batch_type_id")
     * })
     */
    private $batchType;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="company_id")
     * })
     */
    private $company;

    public function getPaymentBatchId(): ?int
    {
        return $this->paymentBatchId;
    }

    public function getBatchDate(): ?\DateTimeInterface
    {
        return $this->batchDate;
    }

    public function setBatchDate(\DateTimeInterface $batchDate): self
    {
        $this->batchDate = $batchDate;

        return $this;
    }

    public function getNumberPayments(): ?int
    {
        return $this->numberPayments;
    }

    public function setNumberPayments(?int $numberPayments): self
    {
        $this->numberPayments = $numberPayments;

        return $this;
    }

    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    public function setTotalAmount($totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getBatchNumber(): ?int
    {
        return $this->batchNumber;
    }

    public function setBatchNumber(?int $batchNumber): self
    {
        $this->batchNumber = $batchNumber;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getBatchType(): ?BatchType
    {
        return $this->batchType;
    }

    public function setBatchType(?BatchType $batchType): self
    {
        $this->batchType = $batchType;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }


}
