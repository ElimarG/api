<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PaymentExtension
 *
 * @ORM\Table(name="payment_extension", indexes={@ORM\Index(name="fk_payment_extension_payment_id", columns={"payment_id"}), @ORM\Index(name="fk_payment_extension_extension_id", columns={"extension_id"})})
 * @ORM\Entity
 */
class PaymentExtension
{
    /**
     * @var int
     *
     * @ORM\Column(name="payment_extension_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentExtensionId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="informed_date", type="datetime", nullable=true)
     */
    private $informedDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Extension
     *
     * @ORM\ManyToOne(targetEntity="Extension")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="extension_id", referencedColumnName="extension_id")
     * })
     */
    private $extension;

    /**
     * @var \Payment
     *
     * @ORM\ManyToOne(targetEntity="Payment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="payment_id")
     * })
     */
    private $payment;

    public function getPaymentExtensionId(): ?int
    {
        return $this->paymentExtensionId;
    }

    public function getInformedDate(): ?\DateTimeInterface
    {
        return $this->informedDate;
    }

    public function setInformedDate(?\DateTimeInterface $informedDate): self
    {
        $this->informedDate = $informedDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getExtension(): ?Extension
    {
        return $this->extension;
    }

    public function setExtension(?Extension $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }


}
