<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PaymentHistory
 *
 * @ORM\Table(name="payment_history", indexes={@ORM\Index(name="fk_payment_history_payment_status_idx", columns={"new_payment_status"}), @ORM\Index(name="fk_payment_history_payment_id_idx", columns={"payment_id"}), @ORM\Index(name="fk_payment_history_payment_batch_id_idx", columns={"payment_batch_id"})})
 * @ORM\Entity
 */
class PaymentHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="payment_history_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentHistoryId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="entry_date", type="datetime", nullable=true)
     */
    private $entryDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \PaymentBatch
     *
     * @ORM\ManyToOne(targetEntity="PaymentBatch")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_batch_id", referencedColumnName="payment_batch_id")
     * })
     */
    private $paymentBatch;

    /**
     * @var \Payment
     *
     * @ORM\ManyToOne(targetEntity="Payment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="payment_id")
     * })
     */
    private $payment;

    /**
     * @var \PaymentStatus
     *
     * @ORM\ManyToOne(targetEntity="PaymentStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="new_payment_status", referencedColumnName="payment_status_id")
     * })
     */
    private $newPaymentStatus;

    public function getPaymentHistoryId(): ?int
    {
        return $this->paymentHistoryId;
    }

    public function getEntryDate(): ?\DateTimeInterface
    {
        return $this->entryDate;
    }

    public function setEntryDate(?\DateTimeInterface $entryDate): self
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getPaymentBatch(): ?PaymentBatch
    {
        return $this->paymentBatch;
    }

    public function setPaymentBatch(?PaymentBatch $paymentBatch): self
    {
        $this->paymentBatch = $paymentBatch;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getNewPaymentStatus(): ?PaymentStatus
    {
        return $this->newPaymentStatus;
    }

    public function setNewPaymentStatus(?PaymentStatus $newPaymentStatus): self
    {
        $this->newPaymentStatus = $newPaymentStatus;

        return $this;
    }


}
