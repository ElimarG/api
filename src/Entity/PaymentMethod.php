<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PaymentMethod
 *
 * @ORM\Table(name="payment_method", indexes={@ORM\Index(name="fk_payment_method_payment_method_type_id", columns={"payment_method_type_id"}), @ORM\Index(name="fk_payment_method_entity_id", columns={"entity_id"})})
 * @ORM\Entity
 */
class PaymentMethod
{
    /**
     * @var int
     *
     * @ORM\Column(name="payment_method_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentMethodId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Entity
     *
     * @ORM\ManyToOne(targetEntity="Entity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="entity_id")
     * })
     */
    private $entity;

    /**
     * @var \PaymentMethodType
     *
     * @ORM\ManyToOne(targetEntity="PaymentMethodType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_method_type_id", referencedColumnName="payment_method_type_id")
     * })
     */
    private $paymentMethodType;

    public function getPaymentMethodId(): ?int
    {
        return $this->paymentMethodId;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getEntity(): ?Entity
    {
        return $this->entity;
    }

    public function setEntity(?Entity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getPaymentMethodType(): ?PaymentMethodType
    {
        return $this->paymentMethodType;
    }

    public function setPaymentMethodType(?PaymentMethodType $paymentMethodType): self
    {
        $this->paymentMethodType = $paymentMethodType;

        return $this;
    }


}
