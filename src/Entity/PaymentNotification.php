<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PaymentNotification
 *
 * @ORM\Table(name="payment_notification", indexes={@ORM\Index(name="fk_payment_notification_payment_batch_id", columns={"payment_batch_id"}), @ORM\Index(name="fk_payment_notification_notification_method_id", columns={"notification_method_id"})})
 * @ORM\Entity
 */
class PaymentNotification
{
    /**
     * @var int
     *
     * @ORM\Column(name="payment_notification_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paymentNotificationId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="informed_date", type="datetime", nullable=true)
     */
    private $informedDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \NotificationMethod
     *
     * @ORM\ManyToOne(targetEntity="NotificationMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_method_id", referencedColumnName="notification_method_id")
     * })
     */
    private $notificationMethod;

    /**
     * @var \PaymentBatch
     *
     * @ORM\ManyToOne(targetEntity="PaymentBatch")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_batch_id", referencedColumnName="payment_batch_id")
     * })
     */
    private $paymentBatch;

    public function getPaymentNotificationId(): ?int
    {
        return $this->paymentNotificationId;
    }

    public function getInformedDate(): ?\DateTimeInterface
    {
        return $this->informedDate;
    }

    public function setInformedDate(?\DateTimeInterface $informedDate): self
    {
        $this->informedDate = $informedDate;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getNotificationMethod(): ?NotificationMethod
    {
        return $this->notificationMethod;
    }

    public function setNotificationMethod(?NotificationMethod $notificationMethod): self
    {
        $this->notificationMethod = $notificationMethod;

        return $this;
    }

    public function getPaymentBatch(): ?PaymentBatch
    {
        return $this->paymentBatch;
    }

    public function setPaymentBatch(?PaymentBatch $paymentBatch): self
    {
        $this->paymentBatch = $paymentBatch;

        return $this;
    }


}
