<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserProcess
 *
 * @ORM\Table(name="user_process", indexes={@ORM\Index(name="fk_user_process_batch_id_idx", columns={"batch_process_id"})})
 * @ORM\Entity
 */
class UserProcess
{
    /**
     * @var int
     *
     * @ORM\Column(name="user_process_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userProcessId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="barcode_id", type="string", length=60, nullable=true)
     */
    private $barcodeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="psf_code", type="string", length=45, nullable=true)
     */
    private $psfCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \BatchProcess
     *
     * @ORM\ManyToOne(targetEntity="BatchProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="batch_process_id", referencedColumnName="batch_process_id")
     * })
     */
    private $batchProcess;

    public function getUserProcessId(): ?int
    {
        return $this->userProcessId;
    }

    public function getBarcodeId(): ?string
    {
        return $this->barcodeId;
    }

    public function setBarcodeId(?string $barcodeId): self
    {
        $this->barcodeId = $barcodeId;

        return $this;
    }

    public function getPsfCode(): ?string
    {
        return $this->psfCode;
    }

    public function setPsfCode(?string $psfCode): self
    {
        $this->psfCode = $psfCode;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getBatchProcess(): ?BatchProcess
    {
        return $this->batchProcess;
    }

    public function setBatchProcess(?BatchProcess $batchProcess): self
    {
        $this->batchProcess = $batchProcess;

        return $this;
    }


}
